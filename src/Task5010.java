public class Task5010 {
    public static void main(String[] args) throws Exception {
        Task5010 foo = new Task5010();
        int total  = foo.sumNumbersV1();
        System.out.println("Tổng từ 1 đến 100 là: " + total);
        total  = Task5010.sumNumbersV1a();
        System.out.println("Tổng từ 1 đến 100 là: " + total);

        int [] number1 = {1, 5, 10};
        int [] number2 = {1,2,3,5,7,9};
        int totalArray1, totalArray2;
        totalArray1 = foo.sumNumbersV2(number1);
        totalArray2 = foo.sumNumbersV2(number2);

        System.out.println("Total arr1: " + totalArray1);
        System.out.println("Total arr2: " + totalArray2);

        Task5010.printHello(24);
        Task5010.printHello(99);

    }

    public int sumNumbersV1() {
        int sum = 0;
        for(int i = 1; i<= 100; i++){
            sum += i;
        }
        return sum;
    }
    public static int sumNumbersV1a() {
        int sum = 0;
        for(int i = 1; i<= 100; i++){
            sum += i;
        }
        return sum;
    }

    public  int sumNumbersV2(int[] numbers) {
        int sum = 0;
        for(int i = 0; i<numbers.length; i++){
            sum += numbers[i];
        }
        return sum;
    }

    public static void  printHello(int  number) {
        if(number % 2 == 1){
            System.out.println("Đây là số lẻ");
        }
        else
        {
            System.out.println("Đây là số chẵn");
        }
    }
}
